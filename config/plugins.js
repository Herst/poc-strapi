'use strict';

const { join } = require('path');

module.exports = () => ({
  graphql: {
    enabled: true,
    config: {
      endpoint: '/graphql',
      playgroundAlways: false,
      apolloServer: {
        tracing: true,
      },
      artifacts: {
        schema: join(__dirname, '..', 'public/generate/schema.graphql'),
      },
    },
  }
})
