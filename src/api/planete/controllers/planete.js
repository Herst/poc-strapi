'use strict';

/**
 * planete controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::planete.planete');
