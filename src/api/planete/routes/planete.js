'use strict';

/**
 * planete router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::planete.planete');
