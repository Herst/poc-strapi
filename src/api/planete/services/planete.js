'use strict';

/**
 * planete service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::planete.planete');
