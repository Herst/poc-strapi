'use strict';

module.exports = {
  /**
   * An asynchronous register function that runs before
   * your application is initialized.
   *
   * This gives you an opportunity to extend code.
   */
  register({ strapi }) {
    const extensionService = strapi.plugin('graphql').service('extension');
    extensionService.shadowCRUD('plugin::i18n.locale').disable();

    extensionService.shadowCRUD('plugin::users-permissions.user').disableQueries();
    extensionService.shadowCRUD('plugin::users-permissions.user').disableMutations();
    extensionService.shadowCRUD('plugin::users-permissions.role').disableQueries();
    extensionService.shadowCRUD('plugin::users-permissions.role').disableMutations();
    extensionService.shadowCRUD('plugin::upload.folder').disableQueries();
    extensionService.shadowCRUD('plugin::upload.folder').disableMutations();
    extensionService.shadowCRUD('plugin::upload.file').disableMutations();
    extensionService.shadowCRUD('plugin::upload.file').disableQueries();
  },


  /**
   * An asynchronous bootstrap function that runs before
   * your application gets started.
   *
   * This gives you an opportunity to set up your data model,
   * run jobs, or perform some special logic.
   */
  bootstrap(/*{ strapi }*/) {},
};
